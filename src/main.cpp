#include <iostream>

#include <libpq-fe.h>

int main(){
  PGPing code_retour_ping;
  code_retour_ping = PQping("host=postgresql.bts-malraux72.net port=5432");
  if(code_retour_ping == PQPING_OK)
  {
    std::cout
      << "La connexion au serveur de base de données a été établie avec succès"
      << std::endl;
  }
  else
  {
    std::cerr
      << "Malheureusement le serveur n'est pas joignable. Vérifier la connectivité"
      << std::endl;
  }
  return 0;
}
